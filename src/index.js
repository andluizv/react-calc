import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { ThemeProvider as ThemeControler } from "./Providers/ThemeContext";

ReactDOM.render(
	<React.StrictMode>
		<ThemeControler>
            <App />
		</ThemeControler>
	</React.StrictMode>,
	document.getElementById("root")
);
