export const themeOne = {
	// Backgrouns
	mainBackground: "hsl(222, 26%, 31%)",
	keypadBackground: "hsl(223, 31%, 20%)",
	screenBackground: "hsl(224, 36%, 15%)",
	htmlBackground: "hsl(221, 27%, 60%)",
	//Keys
	resetKeyBackground: "hsl(225, 21%, 49%)",
	resetKeyShadow: "hsl(224, 28%, 35%)",
	equalKeyBackground: "hsl(6, 63%, 50%)",
	equalKeyShadow: "hsl(6, 70%, 34%)",
	normalKeyBackground: "hsl(30, 25%, 89%)",
	normalKeyShadow: "hsl(28, 16%, 65%)",
	//Text
	textColorPrimary: "hsl(221, 14%, 31%)",
	displayColor: "#fff",
	textColorSecondary: "#fff",
	textColorEqual: "#fff",
};
export const themeTwo = {
	// Backgrouns
	mainBackground: "hsl(0, 0%, 90%)",
	keypadBackground: "hsl(0, 5%, 81%)",
	screenBackground: "hsl(0, 0%, 93%)",
	htmlBackground: "hsl(0, 5%, 35%)",
	//Keys
	resetKeyBackground: "hsl(185, 42%, 37%)",
	resetKeyShadow: "hsl(185, 58%, 25%)",
	equalKeyBackground: "hsl(25, 98%, 40%)",
	equalKeyShadow: "hsl(25, 99%, 27%)",
	normalKeyBackground: "hsl(45, 7%, 89%)",
	normalKeyShadow: "hsl(35, 11%, 61%)",
	//Text
	textColorPrimary: "hsl(60, 10%, 19%)",
	displayColor: "hsl(60, 10%, 19%)",
	textColorSecondary: "#fff",
	textColorEqual: "#fff",
};
export const themeThree = {
	// Backgrouns
	mainBackground: "hsl(268, 75%, 9%)",
	keypadBackground: "hsl(268, 71%, 12%)",
	screenBackground: "hsl(268, 71%, 12%)",
	htmlBackground: "hsl(268, 95%, 30%)",
	//Keys
	resetKeyBackground: "hsl(281, 89%, 26%)",
	resetKeyShadow: "hsl(285, 91%, 52%)",
	equalKeyBackground: "hsl(176, 100%, 44%)",
	equalKeyShadow: "hsl(177, 92%, 70%)",
	normalKeyBackground: "hsl(268, 47%, 21%)",
	normalKeyShadow: "hsl(290, 70%, 36%)",
	//Text
	textColorPrimary: "hsl(52, 100%, 62%)",
	displayColor: "hsl(52, 100%, 62%)",
	textColorSecondary: "#fff",
	textColorEqual: "hsl(198, 20%, 13%)",
};
