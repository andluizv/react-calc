import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    html {
        font-family: 'Spartan', sans-serif;
        background-color: ${(props) => props.theme.htmlBackground}
    }
    button {
        padding: 0 .8rem;
        font-size: 32px;
        border-radius: 4px;
        border: none;
    }
    button:active {
        transform: translateY(4px);
        box-shadow: none;

    }
    input {
        font-family: 'Spartan', sans-serif;
        font-size: 32px;
        color: ${(props) => props.theme.displayColor};
        background-color: ${(props) => props.theme.screenBackground};
        padding: 1rem;
        border-radius: 10px;
        box-sizing: border-box;
    }
    div {
        box-sizing: border-box;
    }
    #root {
        margin: 0;
        display: grid;
        place-items: center;
    }
`;
