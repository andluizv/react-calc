import GlobalStyles from "./Styles/global";
import { ThemeProvider } from "styled-components";
import ThemeSwitcher from "./components/ThemeSwitcher";
import { useTheme } from "./Providers/ThemeContext";
import Calculator from "./components/CalcPackaging";
import Footer from './components/Footer'
import { CalcContainer, Title } from "./styles";

function App() {
	const { themeNumber } = useTheme();
    
   
	return (
		<ThemeProvider theme={themeNumber}>
			<GlobalStyles />
			<CalcContainer>
				<Title>A React Calc 🧮</Title>
				<ThemeSwitcher />
				<Calculator />
                <Footer />
			</CalcContainer>
		</ThemeProvider>
	);
}

export default App;
