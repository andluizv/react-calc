import { createContext, useContext, useState } from "react";
import { themeOne, themeTwo, themeThree } from "../../Styles/themes";

const ThemeContext = createContext();

export const ThemeProvider = ({ children }) => {
	const [themeNumber, setThemeNumber] = useState(themeTwo);

	const handleChangeTheme = (number) => {
		switch (number) {
			case 1:
				setThemeNumber(themeOne);
				break;
			case 2:
				setThemeNumber(themeTwo);
				break;
			case 3:
				setThemeNumber(themeThree);
				break;
			default:
				break;
		}
	};
	return (
		<ThemeContext.Provider
			value={{
				themeNumber,
				handleChangeTheme,
			}}
		>
			{children}
		</ThemeContext.Provider>
	);
};

export const useTheme = () => useContext(ThemeContext);
