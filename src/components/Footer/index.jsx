import {
	FooterContainer,
	LinkLinkedin,
	LinkGitLab,
	SubInfo,
	Link,
} from "./styles";
import { FaLinkedin, FaGitlab } from "react-icons/fa"

const Footer = () => {
    return (
		<FooterContainer>
			<p>Criando por André L S V.</p>

			<div>
				<LinkLinkedin
					href="https://www.linkedin.com/in/andluizv/"
					target="_blank"
					rel="noopener noreferrer"
				>
					<FaLinkedin />
				</LinkLinkedin>{" "}
				<LinkGitLab
					href="https://gitlab.com/andluizv/react-calc"
					target="_blank"
					rel="noopener noreferrer"
				>
					<FaGitlab />
				</LinkGitLab>{" "}
			</div>
			<SubInfo>
				Inspirado no Modelo da{" "}
				<Link
					href="https://www.frontendmentor.io/?ref=challenge"
					rel="noopener noreferrer"
					target="_blank"
				>
					Frontend Mentor
				</Link>
			</SubInfo>
		</FooterContainer>
	);
}
 
export default Footer;