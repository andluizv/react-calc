import styled from "styled-components";

export const FooterContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: column;
    color: ${props => props.theme.displayColor};
    font-weight: bold;
`

export const LinkLinkedin = styled.a`
    color: #0077B5;
    font-size: 1.5rem;
`
export const LinkGitLab = styled.a`
    color: #F48024;
    font-size: 1.5rem;
`

export const SubInfo = styled.p`
    font-size: 8px;
`

export const Link = styled.a`
	&:visited:active, &:link:active, &:visited {
		color: #00f;
	}
`;