import { useState } from "react";
import { useTheme } from "../../Providers/ThemeContext";
import {
	Switcher,
	Container,
	OptionsContainer,
	Options,
	AnounceTheme,
} from "./styles";

const ThemeSwitcher = () => {
	const { handleChangeTheme } = useTheme();
	const [inputNumber, setInputNUmber] = useState(2);

	return (
		<Container>
			<AnounceTheme>Theme</AnounceTheme>
			<div>
				<OptionsContainer>
					<Options>1</Options>
					<Options>2</Options>
					<Options>3</Options>
				</OptionsContainer>
				<Switcher
					type="range"
					min={1}
					max={3}
					value={inputNumber}
					onChange={(e) => {
						handleChangeTheme(Number(e.target.value));
						setInputNUmber(e.target.value);
					}}
				/>
			</div>
		</Container>
	);
};

export default ThemeSwitcher;
