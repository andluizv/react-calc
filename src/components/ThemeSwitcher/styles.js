import styled from "styled-components";

export const Container = styled.div`
	display: flex;
	gap: 15px;
	justify-content: flex-end;
	align-items: center;
`;

export const Switcher = styled.input`
	width: 75px;
	-webkit-appearance: none;
	height: 25px;
	background: ${(props) => props.theme.keypadBackground};
	outline: none;
	padding: 0 5px;
	border-radius: 15px;
	-webkit-transition: 0.2s;
	transition: opacity 0.2s;
	margin: 0;

	&::-webkit-slider-thumb {
		-webkit-appearance: none;
		appearance: none;
		width: 20px;
		height: 20px;
		border-radius: 100%;
		background: ${(props) => props.theme.equalKeyBackground};
		cursor: pointer;
	}
	&::-moz-range-thumb {
		width: 20px;
		height: 20px;
		background: ${(props) => props.theme.equalKeyBackground};
		border: none;
		cursor: pointer;
	}
`;

export const OptionsContainer = styled.div`
	display: flex;
	justify-content: space-between;
	padding: 0 8px;
`;
export const Options = styled.span`
	font-size: 12px;
	color: ${(props) => props.theme.displayColor};
`;

export const AnounceTheme = styled.p`
	color: ${(props) => props.theme.displayColor};
`;
