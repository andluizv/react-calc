import styled from "styled-components";
import { Button } from "../../components/Numbers/styles";

export const OperationButton = styled(Button)`
	grid-area: ${(props) => `${props.grid}`};
`;

export const EqualButton = styled.button`
	background-color: ${(props) => props.theme.equalKeyBackground};
	box-shadow: 0px 4px 0px 0px ${(props) => props.theme.equalKeyShadow};
	color: ${(props) => props.theme.textColorEqual};
	padding: 0 2rem;
	grid-area: equal;
`;

export const ResetButton = styled.button`
	background-color: ${(props) => props.theme.resetKeyBackground};
	box-shadow: 0px 4px 0px 0px ${(props) => props.theme.resetKeyShadow};
	color: ${(props) => props.theme.textColorSecondary};
	font-size: 18px;
	padding: 0.5rem 1rem;
	grid-area: res;
`;

export const BackSpaceButton = styled.button`
	background-color: ${(props) => props.theme.resetKeyBackground};
	box-shadow: 0px 4px 0px 0px ${(props) => props.theme.resetKeyShadow};
	color: ${(props) => props.theme.textColorSecondary};
	grid-area: del;
`;

export const KeysContainer = styled.div`
	background-color: ${(props) => props.theme.keypadBackground};
	padding: 1rem;
	display: grid;
	grid-template-areas: "seven eigth nine del" "four five six add" "one two three minus" "p zero slash plus" "res res equal equal";
	width: 100%;
	gap: 10px;
`;

export const BiggerContainer = styled.div`
	width: 100%;
`;

export const Display = styled.input`
	width: 100%;
	text-align: right;
	overflow: hidden;
	text-overflow: clip;
	margin-bottom: 1rem;
	border-color: ${(props) => props.theme.normalKeyShadow};
`;

export const MakeFocus = styled.button`
	outline: none;
`;
