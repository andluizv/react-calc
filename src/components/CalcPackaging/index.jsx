import React, { useEffect, useRef, useState } from "react";
import Numbers from "../Numbers";
import {
	OperationButton,
	EqualButton,
	ResetButton,
	KeysContainer,
	BackSpaceButton,
	BiggerContainer,
	Display,
	MakeFocus,
} from "./styles";

const Calculator = () => {
	const [display, setDisplay] = useState("0");
	const [elA, setElA] = useState("");
	const [oper, setOper] = useState("");
	const [secondInput, setSecondInput] = useState(false);
	const [error, setError] = useState(false);
	const keyBegginer = useRef();

	useEffect(() => {
		//search alternatives to resolve this:
		keyBegginer.current.focus();
	});

	const handleKeyPress = (key) => {
		const operations = "+-*/";
		switch (true) {
			case key === " ":
				break;
			case !isNaN(key):
				handlePushBtn(key);
				break;
			case key === ".":
				handlePushBtn(key);
				break;
			case key === "Backspace":
				handleBackSpace();
				break;
			case key === "Enter":
				handleResult();
				break;
			case key === "Escape":
				handleClean();
				break;
			case operations.includes(key):
				handleSaveA(key);
				break;

			default:
				return;
		}
	};

	const handlePushBtn = (btn) => {
		if (btn === "." && display.includes(".")) {
			return;
		}
		if (display === "0" || secondInput) {
			setSecondInput(false);
			return setDisplay(btn);
		}
		if (display.length > 10) {
			return;
		}
		setDisplay(display.toString() + btn.toString());
	};

	const handleBackSpace = () => {
		if (display.length > 1) {
			let backValue = display.split("");
			backValue.pop();
			setDisplay(backValue.join(""));
		}
		if (display.length === 1) {
			setDisplay("0");
		}
	};

	const handleSaveA = (op) => {
		setElA(display);
		setOper(op);
		setSecondInput(true);
	};

	const handleResult = () => {
		let a = Number(elA);
		let b = Number(display);
		let result = 0;
		console.log(a);
		console.log(b);
		console.log(oper);
		switch (oper) {
			case "+":
				result = a + b;
				break;
			case "-":
				result = a - b;
				break;
			case "*":
				result = a * b;
				break;
			case "/":
				if (b > 0) {
					result = a / b;
				} else {
					setError(true);
				}

				break;
			default:
				break;
		}
		if (result.toString().length > 10) {
			setDisplay("99999999999");
			setElA("99999999999");
			return;
		}
		if (isNaN(result)) {
			setError(true);
		}
		setDisplay(result.toString());
		setElA(result);
	};

	const handleClean = () => {
		setElA("");
		setOper("");
		setError(false);
		setDisplay("0");
	};

	return (
		<BiggerContainer
			onKeyDown={(e) => {
				e.preventDefault();
				handleKeyPress(e.key, e);
			}}
		>
			<Display
				type="text"
				value={error ? "Error" : display}
				onChange={(e) =>
					setDisplay(isNaN(e.target.value) ? display : e.target.value)
				}
			/>
			<KeysContainer>
				<Numbers func={handlePushBtn} />
				<OperationButton onClick={() => handleSaveA("+")} grid="add">
					+
				</OperationButton>
				<OperationButton onClick={() => handleSaveA("-")} grid="minus">
					-
				</OperationButton>
				<OperationButton onClick={() => handleSaveA("*")} grid="plus">
					×
				</OperationButton>
				<OperationButton onClick={() => handleSaveA("/")} grid="slash">
					÷
				</OperationButton>
				<BackSpaceButton onClick={handleBackSpace}>Del</BackSpaceButton>
				<EqualButton onClick={handleResult}>=</EqualButton>
				<ResetButton onClick={handleClean}>Reset</ResetButton>
				<MakeFocus ref={keyBegginer}></MakeFocus>
			</KeysContainer>
		</BiggerContainer>
	);
};

export default Calculator;
