import styled from "styled-components";

export const Button = styled.button`
	background-color: ${(props) => props.theme.normalKeyBackground};
	box-shadow: 0px 4px 0px 0px ${(props) => props.theme.normalKeyShadow};
	color: ${(props) => props.theme.textColorPrimary};
	grid-area: ${(props) => props.grid};
`;
