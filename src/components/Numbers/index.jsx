import { Button } from "./styles";

const Numbers = ({ func }) => {
	const verboseNumbers = [
		"zero",
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eigth",
		"nine",
	];

	return (
		<>
			<Button
				onClick={(e) => func(e.target.innerText)}
				key={"point"}
				grid="p"
			>
				.
			</Button>
			{verboseNumbers.map((number, ind) => (
				<Button
					onClick={(e) => func(e.target.innerText)}
					grid={number}
					key={number}
				>
					{ind}
				</Button>
			))}
		</>
	);
};

export default Numbers;
