import styled from "styled-components";

export const CalcContainer = styled.div`
	width: clamp(320px, 40%, 400px);
	display: flex;
	flex-direction: column;
	margin: 0;
	background-color: ${(props) => props.theme.mainBackground};
	padding: 1rem;
	border-radius: 10px;
`;

export const Title = styled.h1`
	color: ${(props) => props.theme.displayColor};
`;
